var mlMap = (function ($, coords) {

    var map = '';
    var marker = '';
    var locations = '';
    var testmap = '';
    var map_canvas = $('#map_canvas');
    var name = 'styledMap';
    var styledMapOptions = {};
    var mapOptions = {};
    var image = '';
    var color = '';
    var alertCount = 0;

    getMarkers = function () {
        $.ajax({
            type: 'GET',
            url: constants.ajaxMapMarkers,
            dataType: 'json'
        }).done(function (data) {
            if (data.length == 0) {
            } else {
                locations = data;
                $.each(locations, function (k, v) {
                    addMarker(v);
                });
            };
        }).fail(function (data) {
        });
    }

    getAlertCount = function (alerts) {
        $.each(alerts, function (k, v) {
            alertCount += v;
        });
        return alertCount;
    }

    addMarker = function (loc) {
        if (loc.MarkerType == "DataCenter") {
            image = constants.imagePath + "dc-blue.png";
            alertCount = "";
        } else if (loc.MarkerType == "Service") {
            if (loc.SiteStatus == "Degraded") { image = constants.imagePath + "service-degraded.png"; color = "Black"; alertCount = getAlertCount(loc.Alerts); }
            if (loc.SiteStatus == "Not monitored") { image = constants.imagePath + "service-unmonitored.png"; }
            if (loc.SiteStatus == "Down") { image = constants.imagePath + "service-down.png"; color = "White"; alertCount = getAlertCount(loc.Alerts); }
            if (loc.SiteStatus == "Up") { image = constants.imagePath + "service-up.png"; color = "White"; alertCount = getAlertCount(loc.Alerts); }
        }
       
        marker = new MarkerWithLabel({
            position: new google.maps.LatLng(loc.Latitude, loc.Longitude),
            map: map,
            icon: image,
            labelContent: alertCount,
            labelStyle: { color: color },
            labelAnchor: new google.maps.Point(4, 38)
        });

        addInfoWindow(loc, marker);
        alertCount = 0;
    }

    addInfoWindow = function (loc, marker) {
        var content = '<p>' + loc.Address1 + '</p>' /
            '<p>' + loc.Address2 + '</p>' /
            '<p>' + loc.City + '</p>';

        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }

    getBilling = function () {
        $.ajax({
            type: 'GET',
            url: constants.ajaxBillingAccounts,
            dataType: 'json'
        }).done(function (data) {
            var billingSelect = $('select[name=billing]');
            billingSelect.find('option').remove();
            billingSelect.append('<option />');
            billingSelect.find('option:last-child').text('All Accounts');
            $.each(data, function (k, v) {
                var x = billingSelect.append('<option />');
                x.find('option:last-child').val(v.AccountNumber).text(v.AccountNumber + ' ' + v.Name);
            });
            billingSelect.ddslick({
                background: '#ffffff',
                selectText: "All Accounts",
                onSelected: function (data) {
                    mlMap.filterMarkers(data.selectedData.value);
                }
            })
        });
    }

    getAlerts = function () {
        $.ajax({
            type: 'GET',
            url: constants.ajaxMapAlertType,
            dataType: 'json'
        }).done(function (data) {
            var alertSelect = $('select[name=alerts]');
            alertSelect.find('option').remove();
            alertSelect.append('<option />');
            alertSelect.find('option:last-child').text('Alert Settings');
            $.each(data, function (k, v) {
                var x = alertSelect.append('<option />');
                x.find('option:last-child').val(v).text(v);
            });
            alertSelect.ddslick({
                background: '#ffffff',
                selectText: "Alert Settings",
                onSelected: function (data) {
                    mlMap.filterMarkers(data.selectedData.value);
                },
                width: 200
            })
        })
    }

    return {

        init: function (coords) {

            styledMapOptions.map = map;
            styledMapOptions.name = name;

            mapOptions.zoom = 8;
            mapOptions.center = coords;

            var stylez = [{
                featureType: 'poi',
                stylers: [{
                    visibility: 'off'
                }]
            }, {
                featureType: 'road',
                elementType: 'all',
                stylers: [{
                    visibility: 'off'
                }]
            }, {
                featureType: 'water',
                stylers: [{
                    color: '#808080'
                }]
            }, {
                featureType: 'landscape.natural',
                elementType: 'geometry',
                stylers: [{
                    color: '#FFFFFF'
                }]
            }, {
                featureType: 'landscape.man_made',
                elementType: 'geometry',
                stylers: [{
                    visibility: 'off'
                }]
            }];

            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            testmap = new google.maps.StyledMapType(stylez, mlMap.styledMapOptions);

            map.mapTypes.set('styledMap', testmap);
            map.setMapTypeId('styledMap');

            getMarkers();
            getBilling();
            getAlerts();
        },

        filterMarkers: function (value) {
        }
    }

})(jQuery);