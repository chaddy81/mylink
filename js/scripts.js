$(document).ready(function () {
    Modernizr.load({
        test: Modernizr.touchevents,
        yep: constants.scriptsPath + 'jquery.ui.touch-punch.min.js',
        complete: function () { }
    });

    if ($('.no-csscolumns').length) {
        $('.subnav.products').columnize({ columns: 2 });
        $('.products .column').css('width', '48%');
        $('.products').parent().hover(function () {
            $(this).find('.products').css('visibility', 'visible');
        }, function () {
            $(this).find('.products').css('visibility', 'hidden');
        });
        $('#primary-nav li:first-child').css('marginLeft', '0');
    }

    /* initialize dropdowns */
    $('.state-dropdown').each(function () {
        $(this).ddslick({
            width: 75,
            background: '#ffffff',
            onSelected: function (data) {
                $('input[type=hidden]#StateCode').val(data.selectedData.value);
            }
        });
    });

    $('.occupation-dropdown').each(function () {
        $(this).ddslick({
            width: 165,
            background: '#ffffff',
            onSelected: function (data) {
                $('input[type=hidden]#Occupation').val(data.selectedData.value);
            }
        });
    });

    $('.securityquestion-dropdown').ddslick({
        width: 350,
        background: '#ffffff',
        onSelected: function (data) {
            $('input[type=hidden]#SecurityQuestion').val(data.selectedData.value);
        }
    });

    $('.billingaccount-dropdown').ddslick({
        width: 250,
        background: '#ffffff',
        onSelected: function (data) {
            $('input[type=hidden]#BillingAccountId').val(data.selectedData.value);
        }
    });

    //Need to work on this
    //
    // $('select').each(function() {
    // 			var max_length = 0;
    // 			$(this).children().each(function() {
    // 				var memSpan = $('<span />').css({ display: 'none' });
    // 				memSpan.text('Something Really Cool').appendTo('body');
    // 				max_length = Math.max(max_length, memSpan.width());
    // 			});
    // 			//console.log(max_length);
    // 			$(this).ddslick({
    // 				width: max_length,
    // 				background: '#ffffff'
    // 			});
    // 		});
		
		$('select').each(function() {
			var selId = $(this).attr('id');
			var selSelected = $(this).find("option[selected]");
			var selOptions = $("option", $(this));
			console.log(selId);
			console.log(selSelected);
			console.log(selOptions);
		})

    /* initialize checkboxes */
    /* work around for IE7 and IE8 bug */
    if ($('html').hasClass('lt-ie9')) {
        $('label').click(function () {
            var id = $(this).attr('for');
            $('input[type=checkbox]#' + id).toggleClass('checked');
        });
    } else {
        $('input:checkbox').each(function () {
            $(this).click(function () {
                $(this).toggleClass('checked');
            });
        });
    }

    $('#primary-nav li').hover(function () {
        $(this).find('.subnav').show();
    }, function () {
        $(this).find('.subnav').hide();
    });

    $('.dashboard-tab .open-handle').on('click', function () {
        if (parseInt($(this).parent().css('top'), 10) === 28) {
            $(this).parent().animate({ 'top': '95px' }, 'fast');
        } else {
            $(this).parent().animate({ 'top': '28px' }, 'fast');
        }
    });

    $('.cloud-services .open-handle').on('click', function (e) {
        e.preventDefault();
        if ($(this).parent().hasClass('open')) {
            $('.cloud-services').animate({ left: '-=90%' }, 1000, function () {
                hideOverlay();
            });
            $(this).parent().removeClass('open');
        } else {
            if ($('.manage-locations').hasClass('open')) {
                // $('.manage-locations .close-btn').trigger('click');
                $('.manage-locations').animate({ right: '-90%' }, 1000);
                $('.manage-locations').removeClass('open');
            }
            showOverlay();
            $('.cloud-services').animate({ left: '+=90%' }, 1000);
            $(this).parent().addClass('open');
        }
    });

    $('.cloud-services .close-btn').on('click', function (e) {
        e.preventDefault();
        $('.cloud-services').animate({ left: '-=90%' }, 1000, function () {
            hideOverlay();
        });
        $(this).parent().removeClass('open');
    });

    $('.manage-locations .open-handle').on('click', function (e) {
        e.preventDefault();
        if ($(this).parent().hasClass('open')) {
            $('.manage-locations').animate({ right: '-=90%' }, 1000, function () {
                hideOverlay();
            });
            $(this).parent().removeClass('open');
        } else {
            if ($('.cloud-services').hasClass('open')) {
                $('.cloud-services').animate({ left: '-90%' }, 1000);
                $('.cloud-services').removeClass('open');

            }
            showOverlay();
            $('.manage-locations').animate({ right: '+=90%' }, 1000);
            mlMap.displayMap();
            $(this).parent().addClass('open');
        }
    });

    $('.manage-locations .close-btn').on('click', function (e) {
        e.preventDefault();
        $('.manage-locations').animate({ right: '-=90%' }, 1000, function () {
            hideOverlay();
        });
        $('.manage-locations').removeClass('open');
    });

    $('.nav-tabs li a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $(window).on('resize', function () {
        if ($('.overlay').is(':visible')) {
            showOverlay();
        }
    });

    $("#InvoiceId").change(function () {
        $.ajax({
            type: 'POST',
            url: getInvoiceFilesURL,
            data: { invoiceId: $("#InvoiceId").val() },
            dataType: 'text'
        }).done(function (invoiceFileList) {
            var invoiceFileSelect = $('#InvoiceFileId');
            invoiceFileSelect.find('option').remove();
            invoiceFileSelect.append('<option selected="selected" value="0">Select Format</option>');
            invoiceFileList = $.parseJSON(invoiceFileList);
            $.each(invoiceFileList, function (i, invoiceFile) {
                $('<option>', {
                    value: invoiceFile.InvoiceFileId
                }).html(invoiceFile.Format).appendTo(invoiceFileSelect);
            });
        });
    });
});

function showOverlay() {
    var wrapHeight = $('#wrap').height();
    var wrapWidth = $('#wrap').width();
    var headerHeight = $('#header').outerHeight();
    var footerHeight = $('#footer').outerHeight();

    var overlayHeight = wrapHeight - (headerHeight + footerHeight);
    var overlayWidth = wrapWidth;

    $('.overlay').css({
        'height': overlayHeight,
        'width': overlayWidth,
        'top': headerHeight
    }).show();
}

function hideOverlay() {
    $('.overlay').hide();
}

function SelectAllRadioButtons(value) {
    // ToDo: Convert this method to jQuery.
    for (i = 0; i < document.getElementsByTagName('input').length; i++) {
        if (document.getElementsByTagName('input')[i].type == 'radio' && document.getElementsByTagName('input')[i].value == value) {
            if (!document.getElementsByTagName('input')[i].disabled) {
                document.getElementsByTagName('input')[i].checked = true;
            }
        }
    }
}